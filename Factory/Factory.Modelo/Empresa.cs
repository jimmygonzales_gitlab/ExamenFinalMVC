namespace Factory.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Empresa")]
    public partial class Empresa
    {
        public int Id { get; set; }

        [Required]
        [StringLength(11)]
        public string RUC { get; set; }

        [Required]
        [StringLength(100)]
        public string RazonSocial { get; set; }

        [Required]
        [StringLength(200)]
        public string Direccion { get; set; }

        [StringLength(100)]
        public string Departamento { get; set; }

        [StringLength(100)]
        public string Provincia { get; set; }

        [StringLength(100)]
        public string Distrito { get; set; }

        [StringLength(200)]
        public string Rubro { get; set; }

        [Required]
        [StringLength(128)]
        public string IdUsuario { get; set; }
    }
}
