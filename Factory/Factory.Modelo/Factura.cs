namespace Factory.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Factura")]
    public partial class Factura
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdEmpresa { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(10)]
        public string Numero { get; set; }

        public DateTime FechaEmision { get; set; }

        public DateTime FechaVencimiento { get; set; }

        public DateTime FechaCobro { get; set; }

        [Required]
        [StringLength(11)]
        public string RucCliente { get; set; }

        [Required]
        [StringLength(200)]
        public string RazonSocialCliente { get; set; }

        public double TotalFactura { get; set; }

        public double TotalImpuestosLey { get; set; }

        [StringLength(200)]
        public string FacturaEscaneada { get; set; }
    }
}
