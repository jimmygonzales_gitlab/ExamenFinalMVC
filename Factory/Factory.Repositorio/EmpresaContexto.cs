namespace Factory.Repositorio
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Factory.Modelo;

    public partial class EmpresaContexto : DbContext
    {
        public EmpresaContexto()
            : base("name=CnxFactoryJG")
        {
        }

        public virtual DbSet<Empresa> Empresa { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Empresa>()
                .Property(e => e.RUC)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.RazonSocial)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Direccion)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Departamento)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Provincia)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Distrito)
                .IsUnicode(false);

            modelBuilder.Entity<Empresa>()
                .Property(e => e.Rubro)
                .IsUnicode(false);
        }
    }
}
