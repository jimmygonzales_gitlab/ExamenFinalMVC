namespace Factory.Repositorio
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Factory.Modelo;

    public partial class FacturaContexto : DbContext
    {
        public FacturaContexto()
            : base("name=CnxFactoryJG")
        {
        }

        public virtual DbSet<Factura> Factura { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Factura>()
                .Property(e => e.Numero)
                .IsUnicode(false);

            modelBuilder.Entity<Factura>()
                .Property(e => e.RucCliente)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
