﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Factory.Modelo;

namespace Factory.Repositorio.Impl
{
    public class EmpresaRepositorio:IDisposable
    {
        private readonly DbContext bd;

        private readonly RepositorioGenerico<Empresa> _empresa;

        public EmpresaRepositorio(DbContext bd)
        {
            this.bd = bd;
            _empresa = new RepositorioGenerico<Empresa>(bd);
        }

        public RepositorioGenerico<Empresa> Empresa { get { return _empresa; } }

        public void Commit()
        {
            bd.SaveChanges();
        }
        public void Dispose()
        {
            bd.Dispose();
        }
    }
}
