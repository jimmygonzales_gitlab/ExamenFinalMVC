﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Factory.Modelo;

namespace Factory.Repositorio.Impl
{
    public class FacturaRepositorio: IDisposable
    {
        private readonly DbContext bd;

        private readonly RepositorioGenerico<Factura> _factura;

        public FacturaRepositorio(DbContext bd)
        {
            this.bd = bd;
            _factura = new RepositorioGenerico<Factura>(bd);
        }

        public RepositorioGenerico<Factura> Factura { get { return _factura; } }

        public void Commit()
        {
            bd.SaveChanges();
        }
        public void Dispose()
        {
            bd.Dispose();
        }

    }
}
