﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FactoryWeb.Funcionalidades.ListarEmpresa;
using FactoryWeb.Funcionalidades.RegistrarEmpresa;

namespace FactoryWeb.Controllers
{
    [Authorize]
    public class EmpresaController : Controller
    {
        // GET: Empresa
        public ActionResult Lista()
        {
            using (var listarFactura = new ListarEmpresaHandler())
            {
                return View(listarFactura.Ejecutar());
            }
        }

        [HttpGet]
        public ActionResult Registrar()
        {
            return View(new RegistrarEmpresaViewModel());
        }

        [HttpPost]
        public ActionResult Registrar(RegistrarEmpresaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var registrarEmpresa = new RegistrarEmpresaHandler())
            {
                try
                {
                    registrarEmpresa.Ejecutar(modelo);
                    return RedirectToAction("Lista");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }

            }
        }

    }
}