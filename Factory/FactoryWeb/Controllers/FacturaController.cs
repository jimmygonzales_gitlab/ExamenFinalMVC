﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FactoryWeb.Funcionalidades.ListarFactura;
using FactoryWeb.Funcionalidades.RegistrarFactura;

namespace FactoryWeb.Controllers
{
    [Authorize]
    public class FacturaController : Controller
    {
        // GET: Factura
        public ActionResult Lista()
        {
            using (var listarFactura = new ListarFacturaHandler())
            {
                return View(listarFactura.Ejecutar());
            }
        }

        [HttpGet]
        public ActionResult Registrar()
        {
            return View(new RegistrarFacturaViewModel());
        }


        [HttpPost]
        public ActionResult Registrar(RegistrarFacturaViewModel modelo)
        {
            if (!ModelState.IsValid) return View(modelo);

            using (var registrarFactura = new RegistrarFacturaHandler())
            {
                try
                {
                    registrarFactura.Ejecutar(modelo);
                    return RedirectToAction("Lista");

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    return View(modelo);
                }

            }
        }



    }
}