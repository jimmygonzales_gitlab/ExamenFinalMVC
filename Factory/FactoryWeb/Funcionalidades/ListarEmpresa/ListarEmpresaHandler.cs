﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Factory.Repositorio;
using Factory.Repositorio.Impl;

namespace FactoryWeb.Funcionalidades.ListarEmpresa
{
    public class ListarEmpresaHandler : IDisposable
    {
        private readonly EmpresaRepositorio empresaRepositorio;

        public ListarEmpresaHandler()
        {
            empresaRepositorio = new EmpresaRepositorio(new EmpresaContexto());

        }
        public void Dispose()
        {
            empresaRepositorio.Dispose();
        }

        public IEnumerable<ListarEmpresaViewModel> Ejecutar()
        {
            return empresaRepositorio.Empresa
                     .TrerTodos().Select(e =>
                         new ListarEmpresaViewModel()
                         {
                             Id = e.Id,
                             RUC = e.RUC,
                             RazonSocial = e.RazonSocial,
                             Direccion = e.Direccion,
                             Departamento = e.Departamento,
                             Provincia = e.Provincia,
                             Distrito = e.Distrito,
                             Rubro = e.Rubro,
                             IdUsuario = e.IdUsuario
                         }
                     ).ToList();
        }
    }
}
