﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Factory.Repositorio;
using Factory.Repositorio.Impl;

namespace FactoryWeb.Funcionalidades.ListarFactura
{
    public class ListarFacturaHandler : IDisposable
    {

        private readonly FacturaRepositorio facturaRepositorio;

        public ListarFacturaHandler()
        {
            facturaRepositorio = new FacturaRepositorio(new FacturaContexto());

        }
        public void Dispose()
        {
            facturaRepositorio.Dispose();
        }

        public IEnumerable<ListarFacturaViewModel> Ejecutar()
        {
            return facturaRepositorio.Factura
                     .TrerTodos().Select(e =>
                         new ListarFacturaViewModel()
                         {
                             IdEmpresa = e.IdEmpresa,
                             Numero = e.Numero,
                             FechaEmision = e.FechaEmision,
                             FechaVencimiento = e.FechaVencimiento,
                             FechaCobro = e.FechaCobro,
                             RucCliente = e.RucCliente,
                             RazonSocialCliente = e.RazonSocialCliente,
                             TotalFactura = e.TotalFactura,
                             TotalImpuestosLey = e.TotalImpuestosLey,
                             FacturaEscaneada=e.FacturaEscaneada
                         }
                     ).ToList();
        }

    }
}