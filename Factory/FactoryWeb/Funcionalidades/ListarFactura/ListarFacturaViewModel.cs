﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FactoryWeb.Funcionalidades.ListarFactura
{
    public class ListarFacturaViewModel
    {

        public int IdEmpresa { get; set; }

        public string Numero { get; set; }

        public DateTime FechaEmision { get; set; }

        public DateTime FechaVencimiento { get; set; }

        public DateTime FechaCobro { get; set; }

        public string RucCliente { get; set; }

        public string RazonSocialCliente { get; set; }

        public double TotalFactura { get; set; }

        public double TotalImpuestosLey { get; set; }

        public string FacturaEscaneada { get; set; }

    }
}