﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Factory.Modelo;
using Factory.Repositorio;
using Factory.Repositorio.Impl;

namespace FactoryWeb.Funcionalidades.RegistrarEmpresa
{
    public class RegistrarEmpresaHandler : IDisposable
    {

        private readonly EmpresaRepositorio empresaRepositorio;

        public RegistrarEmpresaHandler()
        {
            empresaRepositorio = new EmpresaRepositorio(new EmpresaContexto());

        }
        public void Dispose()
        {
            empresaRepositorio.Dispose();
        }

        public void Ejecutar(RegistrarEmpresaViewModel modelo)
        {
            Empresa empresa = CreaEmpresa(modelo);

            empresaRepositorio.Empresa.Agregar(empresa);
            empresaRepositorio.Commit();
        }

        private Empresa CreaEmpresa(RegistrarEmpresaViewModel modelo)
        {
            return new Empresa()
            {
                Id = modelo.Id,
                RUC = modelo.RUC,
                RazonSocial = modelo.RazonSocial,
                Direccion = modelo.Direccion,
                Departamento = modelo.Departamento,
                Provincia = modelo.Provincia,
                Distrito = modelo.Distrito,
                Rubro = modelo.Rubro,
                IdUsuario = modelo.IdUsuario
            };
        }
    }
}