﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation.Attributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace FactoryWeb.Funcionalidades.RegistrarEmpresa
{
    [Validator(typeof(RegistrarEmpresaViewModelValidator))]
    public class RegistrarEmpresaViewModel
    {

        public int Id { get; set; }

        public string RUC { get; set; }

        public string RazonSocial { get; set; }

        public string Direccion { get; set; }

        public string Departamento { get; set; }

        public string Provincia { get; set; }

        public string Distrito { get; set; }

        public string Rubro { get; set; }

        public string IdUsuario { get; set; }

        //----------
        public SelectList Departamentos { get; set; }
        public SelectList Provincias { get; set; }
        public SelectList Distritos { get; set; }

        public RegistrarEmpresaViewModel()
        {
            var listaVacia = new List<string>() { "Seleccione..." };
            Departamentos = new SelectList(listaVacia);
            Provincias = new SelectList(listaVacia);
            Distritos = new SelectList(listaVacia);
        }
        //---------

    }
}