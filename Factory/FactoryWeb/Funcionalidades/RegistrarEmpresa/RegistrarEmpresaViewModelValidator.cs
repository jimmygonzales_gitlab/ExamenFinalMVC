﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;

namespace FactoryWeb.Funcionalidades.RegistrarEmpresa
{
    public class RegistrarEmpresaViewModelValidator:AbstractValidator<RegistrarEmpresaViewModel>
    {
        public RegistrarEmpresaViewModelValidator()
        {
            RuleFor(modelo => modelo.RUC)
            .NotEmpty()
            .Must(ValidaRUC)
            .WithMessage("* Debe empezar con 2, tener 11 dígitos y cumplir la validación del Mod. 11 de SUNAT.");

            RuleFor(modelo => modelo.RazonSocial)
            .NotEmpty()
            .WithMessage("* Requerido");

            RuleFor(modelo => modelo.Rubro)
            .NotEmpty() 
            .Must(ValidaRubro)           
            .WithMessage("* Requerido, debe ser Texto.");

        }

        private bool ValidaRUC(RegistrarEmpresaViewModel modelo, string ruc)
        {
            Int64 n = 0;
            bool rpt = false;
            if (ruc.Length == 11)
            {
                if (ruc.Substring(0, 1) == "2")
                {
                    if (Int64.TryParse(ruc, out n))
                    {
                        rpt = ValidacionModuloOnceSUNAT(ruc);
                    }
                }
            }
                
            return rpt;
        }

        public bool ValidacionModuloOnceSUNAT(string ruc)
        {

                int dig01 = Convert.ToInt32(ruc.Substring(0, 1)) * 5;
                int dig02 = Convert.ToInt32(ruc.Substring(1, 1)) * 4;
                int dig03 = Convert.ToInt32(ruc.Substring(2, 1)) * 3;
                int dig04 = Convert.ToInt32(ruc.Substring(3, 1)) * 2;
                int dig05 = Convert.ToInt32(ruc.Substring(4, 1)) * 7;
                int dig06 = Convert.ToInt32(ruc.Substring(5, 1)) * 6;
                int dig07 = Convert.ToInt32(ruc.Substring(6, 1)) * 5;
                int dig08 = Convert.ToInt32(ruc.Substring(7, 1)) * 4;
                int dig09 = Convert.ToInt32(ruc.Substring(8, 1)) * 3;
                int dig10 = Convert.ToInt32(ruc.Substring(9, 1)) * 2;
                int dig11 = Convert.ToInt32(ruc.Substring(10, 1));

                int suma = dig01 + dig02 + dig03 + dig04 + dig05 + dig06 + dig07 + dig08 + dig09 + dig10;
                int residuo = suma % 11;
                int resta = 11 - residuo;

                int digChk;
                if (resta == 10)
                {
                    digChk = 0;
                }
                else if (resta == 11)
                {
                    digChk = 1;
                }
                else
                {
                    digChk = resta;
                }

                if (dig11 == digChk)
                {
                    return true;
                }
                else
                {
                    return false;
                }
        }


        private bool ValidaRubro(RegistrarEmpresaViewModel modelo, string rubro)
        {
            Int64 n = 0;
            bool rpt = false;
            if (!string.IsNullOrEmpty(rubro))
            {
                if (!Int64.TryParse(rubro, out n)) rpt = true;                
            }

            return rpt;
        }


    }

}
