﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Factory.Modelo;
using Factory.Repositorio;
using Factory.Repositorio.Impl;

namespace FactoryWeb.Funcionalidades.RegistrarFactura
{
    public class RegistrarFacturaHandler : IDisposable
    {

        private readonly FacturaRepositorio facturaRepositorio;

        public RegistrarFacturaHandler()
        {
            facturaRepositorio = new FacturaRepositorio(new FacturaContexto());

        }
        public void Dispose()
        {
            facturaRepositorio.Dispose();
        }

        public void Ejecutar(RegistrarFacturaViewModel modelo)
        {
            Factura factura = CreaFactura(modelo);

            facturaRepositorio.Factura.Agregar(factura);
            facturaRepositorio.Commit();
        }

        private Factura CreaFactura(RegistrarFacturaViewModel modelo)
        {
            return new Factura()
            {
                IdEmpresa = modelo.IdEmpresa,
                Numero = modelo.Numero,
                FechaEmision = modelo.FechaEmision,
                FechaVencimiento = modelo.FechaVencimiento,
                FechaCobro = modelo.FechaCobro,
                RucCliente = modelo.RucCliente,
                RazonSocialCliente = modelo.RazonSocialCliente,
                TotalFactura = modelo.TotalFactura,
                TotalImpuestosLey = modelo.TotalImpuestosLey,
                FacturaEscaneada = modelo.FacturaEscaneada
            };
        }


    }
}