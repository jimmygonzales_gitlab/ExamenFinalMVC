﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation.Attributes;
using System.ComponentModel.DataAnnotations;

namespace FactoryWeb.Funcionalidades.RegistrarFactura
{
    [Validator(typeof(RegistrarFacturaViewModelValidator))]

    public class RegistrarFacturaViewModel
    {
        
        public int IdEmpresa { get; set; }

        public string Numero { get; set; }

        public DateTime FechaEmision { get; set; }

        public DateTime FechaVencimiento { get; set; }

        public DateTime FechaCobro { get; set; }

        public string RucCliente { get; set; }

        public string RazonSocialCliente { get; set; }

        public double TotalFactura { get; set; }

        public double TotalImpuestosLey { get; set; }

        public string FacturaEscaneada { get; set; }

    }
}