﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentValidation;


namespace FactoryWeb.Funcionalidades.RegistrarFactura
{
    public class RegistrarFacturaViewModelValidator : AbstractValidator<RegistrarFacturaViewModel>
    {

        public RegistrarFacturaViewModelValidator()
        {
            RuleFor(modelo => modelo.FechaEmision)
                .NotEmpty()                
                .WithMessage("* Requerido, Fecha de Emisión debe ser menor o igual que la Fecha de Vencimiento");

            RuleFor(modelo => modelo.FechaVencimiento)
                .NotEmpty()
                .Must(ValidafechaVencimienton)
                .WithMessage("* Requerido, Fecha de Vencimiento debe ser menor o igual que la Fecha de cobro");

            RuleFor(modelo => modelo.FechaCobro)
                .NotEmpty()
                .GreaterThanOrEqualTo(DateTime.Now.Date)
                .WithMessage("* Requerido, Debe ser mayor o igual a la fecha Actual.");

            RuleFor(modelo => modelo.RucCliente)
                .NotEmpty()
                .Must(ValidaRUC)
                .WithMessage("* Debe empezar con 2, tener 11 dígitos y cumplir la validación del Mod. 11 de SUNAT.");

            RuleFor(modelo => modelo.TotalFactura)
                .GreaterThan(0)
                .WithMessage("* Requerido.");

            RuleFor(modelo => modelo.TotalImpuestosLey)
                .GreaterThan(0)
                .Must(ValidaTotalImpuestoLey)
                .WithMessage("* Debe ser el 18% del Total de la Factura");


        }

        private bool ValidafechaEmision(RegistrarFacturaViewModel modelo, DateTime fechaEmision)
        {
            return (fechaEmision <= (modelo.FechaVencimiento));
        }
        private bool ValidafechaVencimienton(RegistrarFacturaViewModel modelo, DateTime fechaVencimiento)
        {
            return (fechaVencimiento <= (modelo.FechaCobro));
        }


        private bool ValidaRUC(RegistrarFacturaViewModel modelo, string ruc)
        {
            Int64 n = 0;
            bool rpt = false;
            if (ruc.Length == 11)
            {
                if (ruc.Substring(0, 1) == "2")
                {
                    if (Int64.TryParse(ruc, out n))
                    {
                        rpt = ValidacionModuloOnceSUNAT(ruc);
                    }
                }
            }

            return rpt;
        }

        public bool ValidacionModuloOnceSUNAT(string ruc)
        {

            int dig01 = Convert.ToInt32(ruc.Substring(0, 1)) * 5;
            int dig02 = Convert.ToInt32(ruc.Substring(1, 1)) * 4;
            int dig03 = Convert.ToInt32(ruc.Substring(2, 1)) * 3;
            int dig04 = Convert.ToInt32(ruc.Substring(3, 1)) * 2;
            int dig05 = Convert.ToInt32(ruc.Substring(4, 1)) * 7;
            int dig06 = Convert.ToInt32(ruc.Substring(5, 1)) * 6;
            int dig07 = Convert.ToInt32(ruc.Substring(6, 1)) * 5;
            int dig08 = Convert.ToInt32(ruc.Substring(7, 1)) * 4;
            int dig09 = Convert.ToInt32(ruc.Substring(8, 1)) * 3;
            int dig10 = Convert.ToInt32(ruc.Substring(9, 1)) * 2;
            int dig11 = Convert.ToInt32(ruc.Substring(10, 1));

            int suma = dig01 + dig02 + dig03 + dig04 + dig05 + dig06 + dig07 + dig08 + dig09 + dig10;
            int residuo = suma % 11;
            int resta = 11 - residuo;

            int digChk;
            if (resta == 10)
            {
                digChk = 0;
            }
            else if (resta == 11)
            {
                digChk = 1;
            }
            else
            {
                digChk = resta;
            }

            if (dig11 == digChk)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool ValidaTotalImpuestoLey(RegistrarFacturaViewModel modelo, double totalImpuestosLey)
        {
            return (totalImpuestosLey == (modelo.TotalFactura * (0.18)));
        }


    }
}