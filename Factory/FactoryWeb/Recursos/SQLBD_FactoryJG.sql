---
go
use master
go

create database FactoryJG

go 

use FactoryJG

go

create table Empresa(
Id int identity(1,1) not null,
RUC char(11) not null,
RazonSocial varchar(100) not null,
Direccion varchar(200) not null,
Departamento varchar(100),
Provincia varchar(100),
Distrito varchar(100),
Rubro varchar(200),
IdUsuario nvarchar(128) not null,
)

go
Alter Table Empresa Add Constraint PK_Empresa_Id  Primary Key (Id)
go

create table Factura(
IdEmpresa int not null,
Numero varchar(10) not null,
FechaEmision datetime not null,
FechaVencimiento datetime not null,  
FechaCobro datetime not null,  
RucCliente char(11) not null,
RazonSocialCliente nvarchar(200) not null,
TotalFactura float not null,
TotalImpuestosLey float not null,
FacturaEscaneada nvarchar(200)
)
go
Alter Table Factura Add Constraint PK_Factura_IdEmpresa_Numero  Primary Key (IdEmpresa,Numero)
go
Alter Table Factura Add Constraint FK_Factura_Empresa_IdEmpresa  Foreign Key (IdEmpresa) References Empresa(Id)



